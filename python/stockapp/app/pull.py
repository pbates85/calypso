import requests
import json
import csv


API_KEY = ''

def get_all_nasdaq():
    print.type(print_tickers('./app/tickers.csv'))


def get_stock(stock):
    response = requests.get(
        f'https://www.alphavantage.co/query?function=LISTING_STATUS&apikey={API_KEY}&exchange=NASDAQ')
    try:
        # replace the "demo" apikey below with your own key from https://www.alphavantage.co/support/#api-key
        url = f'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol={stock}&interval=5min&apikey=demo'
        r = requests.get(url)
        data = r.json()
        print(data)
    except json.JSONDecodeError:
        data = {}
    return data


def print_tickers(filename):
    with open(filename, 'r') as file:
        reader = csv.reader(file)
        next(reader)  # skip header row
        for row in reader:
            print(row[0])



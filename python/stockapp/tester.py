import pika
import json

# Define the dictionary to be sent
data = {'symbol': 'AAPL', 'price': 139.98}

try:
    # Connect to RabbitMQ
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()

    # Set up the exchange and queue
    channel.exchange_declare(exchange='stock-data-exchange', exchange_type='topic')
    channel.queue_declare(queue='stock-data-queue')
    channel.queue_bind(queue='stock-data-queue', exchange='stock-data-exchange', routing_key='*.important.*')

    # Publish the message
    channel.basic_publish(exchange='stock-data-exchange', routing_key='foo.important.bar', body=json.dumps(data))

    # Close the connection
    connection.close()

except pika.exceptions.AMQPConnectionError as e:
    print("Connection error: ", str(e))

except pika.exceptions.AMQPChannelError as e:
    print("Channel error: ", str(e))

except Exception as e:
    print("Error: ", str(e))




# def kafka_send(datadict):
#     while True:
#         try:
#             producer = KafkaProducer(
#                 bootstrap_servers='localhost:9092',
#                 value_serializer=lambda x: json.dumps(x.decode('utf-8')).encode('utf-8')
#             )
#             message = json.dumps(datadict)
#             print("Connection Open")
#             producer.send('stock-data', value=message.encode('utf-8'))
#             print("Data Submitted")
#             producer.close()
#             print("Connection Closed")
#             break
#         except NoBrokersAvailable:
#             print("Connection Failed: NoBrokersAvailable")
#             print("Retrying in 5 seconds...")
#             time.sleep(5)
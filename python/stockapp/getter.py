import pika
import json
import threading


def consume_data():
    try:
        # Connect to RabbitMQ
        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        channel = connection.channel()

        # Set up the exchange and queue
        channel.exchange_declare(exchange='stock-data-exchange', exchange_type='topic')
        channel.queue_declare(queue='stock-data-queue')
        channel.queue_bind(queue='stock-data-queue', exchange='stock-data-exchange', routing_key='*.important.*')

        # Define the callback function
        def callback(ch, method, properties, body):
            data = json.loads(body)
            print("Received data: ", data)

        # Consume the messages
        channel.basic_consume(queue='stock-data-queue', on_message_callback=callback, auto_ack=True)
        channel.start_consuming()

    except pika.exceptions.AMQPConnectionError as e:
        print("Connection error: ", str(e))

    except pika.exceptions.AMQPChannelError as e:
        print("Channel error: ", str(e))

    except Exception as e:
        print("Error: ", str(e))

# Start the consumer in a separate thread
consumer_thread = threading.Thread(target=consume_data)
consumer_thread.start()



# import pika
# import json
#
# try:
#     # Connect to RabbitMQ
#     connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
#     channel = connection.channel()
#
#     # Set up the exchange and queue
#     channel.exchange_declare(exchange='stock-data-exchange', exchange_type='topic')
#     channel.queue_declare(queue='stock-data-queue')
#     channel.queue_bind(queue='stock-data-queue', exchange='stock-data-exchange', routing_key='*.important.*')
#
#     # Define the callback function
#     def callback(ch, method, properties, body):
#         data = json.loads(body)
#         print("Received data: ", data)
#
#     # Consume the messages
#     channel.basic_consume(queue='stock-data-queue', on_message_callback=callback, auto_ack=True)
#     channel.start_consuming(timeout=1)
#
# except pika.exceptions.AMQPConnectionError as e:
#     print("Connection error: ", str(e))
#
# except pika.exceptions.AMQPChannelError as e:
#     print("Channel error: ", str(e))
#
# except Exception as e:
#     print("Error: ", str(e))
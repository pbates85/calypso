from constants import serverConstants as c
import app.config as web


def main():
    web.app.run(host=c.HOST)


if __name__ == '__main__':
    main()

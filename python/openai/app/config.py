from flask import Flask, request, abort, jsonify
from flask_cors import CORS
import openai

app = Flask(__name__)
CORS(app)


@app.route('/message', methods=['POST'])
def process_message():
    if request.method == 'POST':
        data = request.get_json()
        print(data['prompt'])
        user_question = data['prompt']
        ai_response = chat(user_question)
        return jsonify({'message': ai_response})


openai.api_key = open("key.txt", "r").read().strip('\n')


# Take in input(inp) and post to openAI api return the payload to the user
def chat(inp, role="user"):
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[{"role": role, "content": f"{inp}"}],
        max_tokens=1024,
        n=1,
        stop=None,
        temperature=0.5,
    )

    reply_content = response.choices[0].message.content
    print(reply_content, flush=True)
    return(reply_content)
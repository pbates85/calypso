# GitOps/DevOps demonstration repo
---
### Local Dev
- start up docker mysql connection
```bash
docker run --name mysql -d \
    -p 3306:3306 \
    -e MYSQL_ROOT_PASSWORD=change-me \
    --restart unless-stopped \
    mysql:8
```
- log in create database schema 
- update application.properties accordingly
- Docker Compose option ``docker-compose up``
---
### Kind
- start docker daemon
- create a test cluster ``kind create cluster``
- switch context if not done automatically ``kubectl cluster-info --context kind-devops``
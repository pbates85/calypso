package com.trident.calypso.sftp;

import org.junit.jupiter.api.Test;

public class SftpTest {

    @Test
    public void upload() {
        new UpAndDownload().upload();
    }

    @Test
    public void download() {
        String download = new UpAndDownload().download();
        System.out.println("Downloaded text\n" + download);
    }
}

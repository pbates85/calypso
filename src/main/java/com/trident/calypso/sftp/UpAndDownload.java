package com.trident.calypso.sftp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import org.springframework.integration.sftp.session.SftpSession;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

//Service and Bean annotations need to be added to use with an SFTP instance

public class UpAndDownload {

    @Value("${SFTP_HOST:localhost}")
    private String hostName;

    @Value("${SFTP_PORT:22}")
    private Integer hostPort;

    @Value("${SFTP_USER:foo}")
    private String sftpUser;

    @Value("${SFTP_PASSWORD:pass}")
    private String sftpPassword;


    private DefaultSftpSessionFactory gimmeFactory(){
        DefaultSftpSessionFactory factory = new DefaultSftpSessionFactory();
        // docker container with port mapping change
        factory.setHost(hostName);
        factory.setPort(hostPort);
        factory.setAllowUnknownKeys(true);
        factory.setUser(sftpUser);
        factory.setPassword(sftpPassword);
        return factory;
    }


    public void upload(){
        // connection to sftp server
        SftpSession session = gimmeFactory().getSession();
        // file to upload
        InputStream resourceAsStream =
                UpAndDownload.class.getClassLoader().getResourceAsStream("mytextfile.txt");
        try {
            // write file to sftp with unique name
            session.write(resourceAsStream, "upload/mynewfile" + LocalDateTime.now() +".txt");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        session.close();
    }

//    @Bean // File needs to be present in the sftp folder or bean will fail on boot
    public String download(){
        SftpSession session = gimmeFactory().getSession();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            session.read("upload/downloadme.txt", outputStream);
            return new String(outputStream.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

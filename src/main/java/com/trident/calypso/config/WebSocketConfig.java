package com.trident.calypso.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic");
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/ws");
    }

}
// need to allow origins on stomp endpoints registry.addEndpoint("/ws").setAllowedOrigins("*"); currently off for security

// Example
//@Controller
//public class YarnResource {
//
//    @MessageMapping("/public")
//    @SendTo("/topic/public")
//    public YarnMessage sendMessage(
//            YarnMessage message
//    ) {
//        System.out.println("Public Hit: " + message.getContent());
//        return new YarnMessage(HtmlUtils.htmlEscape(message.getContent()));
//    }
//}
package com.trident.calypso.user.captain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CaptainRepository extends JpaRepository<Captain, Long> {
    Captain findCaptainByEmail(String email);
}

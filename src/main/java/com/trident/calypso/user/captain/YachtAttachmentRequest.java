package com.trident.calypso.user.captain;

public class YachtAttachmentRequest {

    private String email;
    private String mmsi;

    public YachtAttachmentRequest() {
        // Empty constructor required for deserialization
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMmsi() {
        return mmsi;
    }

    public void setMmsi(String mmsi) {
        this.mmsi = mmsi;
    }
}

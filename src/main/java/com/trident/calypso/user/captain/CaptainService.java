package com.trident.calypso.user.captain;

import java.util.List;

public interface CaptainService {

    Captain register(String firstName, String lastName, String email, Long phoneNumber);

    List<Captain> getCaptains();

    Captain findCaptainByEmail(String email);

    Captain attachYacht(String captainEmail, Long mmsi);

    Captain detachYacht(String captainEmail);

    Captain updateCaptain(Captain exsistingCaptain);

}

package com.trident.calypso.user.captain.implementation;

import com.trident.calypso.user.captain.Captain;
import com.trident.calypso.user.captain.CaptainRepository;
import com.trident.calypso.user.captain.CaptainService;
import com.trident.calypso.yacht.Yacht;
import com.trident.calypso.yacht.YachtRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class CaptainServiceImpl implements CaptainService {

    private CaptainRepository captainRepository;
    private YachtRepository yachtRepository;

    @Autowired
    public CaptainServiceImpl(CaptainRepository captainRepository, YachtRepository yachtRepository){
        this.captainRepository = captainRepository;
        this.yachtRepository = yachtRepository;
    }

    @Override
    public Captain register(String firstName, String lastName, String email, Long phoneNumber) {
        Captain captain = new Captain();
        captain.setFirstName(firstName);
        captain.setLastName(lastName);
        captain.setEmail(email);
        captain.setPhoneNumber(phoneNumber);
        captainRepository.save(captain);
        return captain;
    }

    @Override
    public List<Captain> getCaptains() {
        return captainRepository.findAll();
    }

    @Override
    public Captain findCaptainByEmail(String email) {
        return captainRepository.findCaptainByEmail(email);
    }

    @Override
    public Captain attachYacht(String captainEmail, Long mmsi) {
        Captain captain = findCaptainByEmail(captainEmail);
        Yacht yacht = yachtRepository.findByMmsi(mmsi);
        captain.setYacht(yacht);
        return captain;
    }

    @Override
    public Captain detachYacht(String captainEmail) {
        Captain captain = findCaptainByEmail(captainEmail);
        captain.setYacht(null);
        return captain;
    }

    @Override
    public Captain updateCaptain(Captain exsistingCaptain) {
        Captain updatedCaptain = captainRepository.save(exsistingCaptain);
        return updatedCaptain;
    }
}

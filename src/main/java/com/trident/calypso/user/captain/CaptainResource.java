package com.trident.calypso.user.captain;


import com.trident.calypso.user.User;
import com.trident.calypso.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(path = { "/api/captain"})
public class CaptainResource {

    private CaptainService captainService;

    private UserService userService;

    public CaptainResource(
            CaptainService captainService,
            UserService userService
    ) {
        this.captainService = captainService;
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<Captain> registerCaptain(
            @RequestBody Captain captain
    ){
        Captain newCaptain = captainService.register(captain.getFirstName(), captain.getLastName(), captain.getEmail(), captain.getPhoneNumber());
        return new ResponseEntity<>(newCaptain, OK);
    }

    @GetMapping("/list")
    public ResponseEntity<List<Captain>> getAllCaptains(){
        List<Captain> captains = captainService.getCaptains();

        return new ResponseEntity<>(captains, OK);
    }

    @GetMapping("/email/{email}")
    public ResponseEntity<Captain> getCaptainByEmail(@PathVariable("email") String email){
        Captain captain = captainService.findCaptainByEmail(email);
        return new ResponseEntity<>(captain, OK);
    }

    @PostMapping("/attach-yacht")
    public ResponseEntity<Captain> attachYacht(@RequestBody YachtAttachmentRequest request) {
        String email = request.getEmail();
        Long mmsi = Long.parseLong(request.getMmsi());
        Captain captain = captainService.attachYacht(email, mmsi);
        return new ResponseEntity<>(captain, HttpStatus.OK);
    }

    @PostMapping("/detach-yacht/{email}")
    public ResponseEntity<Captain> detachYacht(@PathVariable("email")String email){
        // this needs to be updated so that it can only be changed if the request comes from that captain
        Captain captain = captainService.detachYacht(email);
        return new ResponseEntity<>(captain, OK);
    }

    // get captain by username
    @GetMapping("/user/{username}")
    public ResponseEntity<Captain> getCaptByUsername(@PathVariable("username")String username){

        User user = userService.findUserByUsername(username);
        Captain captain = captainService.findCaptainByEmail(user.getEmail());
        return new ResponseEntity<>(captain, OK);
    }
}

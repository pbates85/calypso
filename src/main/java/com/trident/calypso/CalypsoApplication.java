package com.trident.calypso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.File;

import static com.trident.calypso.contstant.FileConstant.USER_FOLDER;

@EnableCaching
@SpringBootApplication
public class CalypsoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalypsoApplication.class, args);
		// create user folder on container startup
		new File(USER_FOLDER).mkdirs();
	}
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

}

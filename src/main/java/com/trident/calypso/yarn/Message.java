package com.trident.calypso.yarn;

import com.trident.calypso.user.User;
import jakarta.persistence.*;

import java.time.LocalDateTime;


@Entity
@Table(name = "message")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    @ManyToOne
    private YarnBoard recipientYarnBoard;  // Added 'recipientYarnBoard' property

    @ManyToOne
    private User sender;

    @Column(length = 1000)
    private String content;

    @Column(nullable = false)
    private LocalDateTime timestamp;

    public Message(Long id, YarnBoard recipientYarnBoard, User sender, String content, LocalDateTime timestamp) {
        this.id = id;
        this.recipientYarnBoard = recipientYarnBoard;
        this.sender = sender;
        this.content = content;
        this.timestamp = timestamp;
    }

    public Message() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public YarnBoard getRecipientYarnBoard() {
        return recipientYarnBoard;
    }

    public void setRecipientYarnBoard(YarnBoard recipientYarnBoard) {
        this.recipientYarnBoard = recipientYarnBoard;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
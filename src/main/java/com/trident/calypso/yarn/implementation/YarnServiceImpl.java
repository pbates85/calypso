package com.trident.calypso.yarn.implementation;

import com.trident.calypso.exception.domain.UserNotFoundException;
import com.trident.calypso.user.User;
import com.trident.calypso.user.UserRepository;
import com.trident.calypso.yarn.*;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


@Service
public class YarnServiceImpl implements YarnService {

    private final BoardRepository boardRepository;
    private final UserRepository userRepository;

    public YarnServiceImpl(BoardRepository boardRepository, UserRepository userRepository) {
        this.boardRepository = boardRepository;
        this.userRepository = userRepository;

    }

    @Override
    public void postMessage(String senderUname, String recipientUname, String content) throws UserNotFoundException {
        // get sender and receiver objects
        User sender = userRepository.findUserByUsername(senderUname);
        User recipient = userRepository.findUserByUsername(recipientUname);

        YarnBoard recipientYarnBoard = boardRepository.findByOwner(recipient);

        if (recipientYarnBoard == null) {
            // If the recipient doesn't have a yarn board, create a new one
            recipientYarnBoard = new YarnBoard();
            recipientYarnBoard.setOwner(recipient);
            boardRepository.save(recipientYarnBoard);
        }


        // Create a new message
        Message message = new Message();
        message.setSender(sender);
        message.setRecipientYarnBoard(recipientYarnBoard);
        message.setContent(content);
        message.setTimestamp(LocalDateTime.now());

        // Save the message to the recipient's yarn board
        recipientYarnBoard.getReceivedMessages().add(message);
        boardRepository.save(recipientYarnBoard);

    }

    @Override
    public List<YarnMessageResponseDto> getMessagesByUser(String username) {
        YarnBoard board = findUserBoard(username);
        List<Message> messages = board.getReceivedMessages();
        // Map messages to YarnMessageResponseDto
        List<YarnMessageResponseDto> responseDtoList = new ArrayList<>();
        for (Message message : messages) {
            YarnMessageResponseDto dto = new YarnMessageResponseDto();
            dto.setSender(message.getSender().getUsername());
            dto.setRecipient(message.getRecipientYarnBoard().getOwner().getUsername());
            dto.setMessage(message.getContent().toString());
            // Format the timestamp as a string
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String formattedTimestamp = message.getTimestamp().format(formatter);
            dto.setTimestamp(formattedTimestamp);
            responseDtoList.add(dto);
        }
        return responseDtoList;
    }

    public YarnBoard findUserBoard(String uname){
        User user = userRepository.findUserByUsername(uname);
        return boardRepository.findByOwner(user);
    }

}

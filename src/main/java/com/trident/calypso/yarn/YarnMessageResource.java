package com.trident.calypso.yarn;

import com.trident.calypso.exception.domain.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping("/api/yarn")
public class YarnMessageResource {

    private final YarnService yarnService;

    @Autowired
    public YarnMessageResource(YarnService yarnService) {
        this.yarnService = yarnService;
    }


    @PostMapping("/write")
    public HttpStatus writeToBoard(@RequestBody YarnMessageRequestDto requestDto) throws UserNotFoundException {
        yarnService.postMessage(requestDto.getSender(), requestDto.getRecipient(), requestDto.getContent());
        return HttpStatus.OK;
    }

    @GetMapping("/messages/{username}")
    public ResponseEntity<List<YarnMessageResponseDto>> usersMessages(@PathVariable("username") String username) {


        List<YarnMessageResponseDto> responseDtoList = yarnService.getMessagesByUser(username);
        return ResponseEntity.ok(responseDtoList);
    }


}

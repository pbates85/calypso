package com.trident.calypso.yarn;

import com.trident.calypso.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRepository extends JpaRepository<YarnBoard, Long> {

    YarnBoard findByOwner(User owner);
}

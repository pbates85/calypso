package com.trident.calypso.yarn;

import com.trident.calypso.exception.domain.UserNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface YarnService {

    void postMessage(String sender, String recipient, String content) throws UserNotFoundException;

    List<YarnMessageResponseDto> getMessagesByUser(String username);

}

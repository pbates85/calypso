package com.trident.calypso.yarn;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.trident.calypso.user.User;
import jakarta.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "yarn_board")
public class YarnBoard {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    @OneToOne
    private User owner;  // Users own board

    @JsonIgnore
    @OneToMany(mappedBy = "recipientYarnBoard", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Message> receivedMessages = new ArrayList<>();

    public YarnBoard(Long id, User owner, List<Message> receivedMessages) {
        this.id = id;
        this.owner = owner;
        this.receivedMessages = receivedMessages;
    }

    public YarnBoard() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<Message> getReceivedMessages() {
        return receivedMessages;
    }

    public void setReceivedMessages(List<Message> receivedMessages) {
        this.receivedMessages = receivedMessages;
    }
}

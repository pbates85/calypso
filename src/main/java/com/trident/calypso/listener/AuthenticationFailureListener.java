package com.trident.calypso.listener;

import com.trident.calypso.user.login.LoginAttemptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationFailureListener {
    private LoginAttemptService loginAttemptService;

    @Autowired
    public AuthenticationFailureListener(LoginAttemptService loginAttemptService) {
        this.loginAttemptService = loginAttemptService;
    }
    // https://docs.spring.io/spring-security/reference/servlet/authentication/events.html
    @EventListener
    public void onAuthenticationFailure(AuthenticationFailureBadCredentialsEvent event) {
        // Pull principal that contains the username
        Object principal = event.getAuthentication().getPrincipal();
        // safe check
        if(principal instanceof String) {
            String username = (String) event.getAuthentication().getPrincipal();
            // add the user to the cache
            loginAttemptService.addUserToLoginAttemptCache(username);
        }
    }
}

package com.trident.calypso.logbook;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PassageAppService {

    // need user, passage id, application info
    public void apply(String username, Long passageId, String communication, String certifications, String experience, String availableTime, String message);

    public List<PassageApp> getAllApps(Long passageID);

    public List<PassageApp> getAllAppsByUser(String username);

}

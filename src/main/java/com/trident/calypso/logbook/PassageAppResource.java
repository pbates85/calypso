package com.trident.calypso.logbook;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/applications")
public class PassageAppResource {

    private final PassageAppService passageAppService;

    public PassageAppResource(
            PassageAppService passageAppService)
    {
        this.passageAppService = passageAppService;
    }


    @PostMapping("/{username}/{passageid}")
    public HttpStatus applyToPassage(@PathVariable String username, @PathVariable Long passageid, @RequestBody PassageApp application) {
        passageAppService.apply(
                username,
                passageid,
                application.getCommunication(),
                application.getCertifications(),
                application.getExperience(),
                application.getAvailableTime(),
                application.getMessage()
        );
        return HttpStatus.CREATED;
    }

    @GetMapping("/user/{username}")
    public List<PassageApp> getAllPassagesByUsername(@PathVariable String username) throws IOException {
        return passageAppService.getAllAppsByUser(username);
    }
}

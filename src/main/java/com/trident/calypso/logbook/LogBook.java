package com.trident.calypso.logbook;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.trident.calypso.user.User;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "log_book")
public class LogBook {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    @OneToOne
    private User owner;  // Users own log book


    @JsonIgnore
    @OneToMany(mappedBy = "captainsLogBook", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Passage> postedPassages = new ArrayList<>();

    public LogBook(){};

    public LogBook(Long id, User owner, List<Passage> postedPassages) {
        this.id = id;
        this.owner = owner;
        this.postedPassages = postedPassages;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<Passage> getPostedPassages() {
        return postedPassages;
    }

    public void setPostedPassages(List<Passage> postedPassages) {
        this.postedPassages = postedPassages;
    }
}

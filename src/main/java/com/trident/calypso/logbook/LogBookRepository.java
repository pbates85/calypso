package com.trident.calypso.logbook;

import com.trident.calypso.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogBookRepository extends JpaRepository<LogBook, Long> {

    LogBook findByOwner(User owner);
}

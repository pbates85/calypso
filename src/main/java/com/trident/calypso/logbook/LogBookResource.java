package com.trident.calypso.logbook;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/api/logbook")
@RestController
public class LogBookResource {

    private final LogBookService logBookService;

    public LogBookResource(LogBookService logBookService) {
        this.logBookService = logBookService;
    }

    @GetMapping("/passages/{username}")
    public ResponseEntity<List<Passage>> usersPassages(
            @PathVariable("username") String username
    ){
        List<Passage> passages = logBookService.getPassagesByUser(username);
        if (passages.isEmpty()) {
            return ResponseEntity.noContent().build(); // Return 204 No Content if there are no passages
        }
        return ResponseEntity.ok(passages); // Return the list of passages with 200 OK status
    }


}

package com.trident.calypso.logbook;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public interface PassageService {

    void writePassage(String username, String title, String description, String departureDate, String departureLocation, String expenses, String expectations, String destination, boolean isActive);

    void editPassage(String username, Long passageID, String title, String description, String departureDate, String departureLocation, String expenses, String expectations, String destination, boolean isActive);

    void deletePassage(String username, Long passageID) throws IOException;

    List<Passage> getAllPassages();

    Passage completePassage(String username, Long passageID) throws IOException;



    List<Passage> getAllActivePassages();

    List<Passage> getAllByUserId(Long userId);


}

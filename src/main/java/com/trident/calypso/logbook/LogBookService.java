package com.trident.calypso.logbook;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface LogBookService {

    List<Passage> getPassagesByUser(String username);
}

package com.trident.calypso.logbook;

import com.trident.calypso.user.User;
import jakarta.persistence.*;

@Entity
@Table(name = "passage_app")
public class PassageApp {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    private String username;

    private String applicantName;

    private Long passageID;

    private String communication;

    private String certifications;

    private String experience;

    private String availableTime;

    @Column(length = 1000)
    private String Message;

    public PassageApp(){};

    public PassageApp(Long id, String username, String applicantName, Long passageID, String communication, String certifications, String experience, String availableTime, String message) {
        this.id = id;
        this.username = username;
        this.applicantName = applicantName;
        this.passageID = passageID;
        this.communication = communication;
        this.certifications = certifications;
        this.experience = experience;
        this.availableTime = availableTime;
        Message = message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getPassageID() {
        return passageID;
    }

    public void setPassageID(Long passageID) {
        this.passageID = passageID;
    }

    public String getCommunication() {
        return communication;
    }

    public void setCommunication(String communication) {
        this.communication = communication;
    }

    public String getCertifications() {
        return certifications;
    }

    public void setCertifications(String certifications) {
        this.certifications = certifications;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getAvailableTime() {
        return availableTime;
    }

    public void setAvailableTime(String availableTime) {
        this.availableTime = availableTime;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}

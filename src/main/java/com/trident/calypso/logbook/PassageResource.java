package com.trident.calypso.logbook;

import com.trident.calypso.user.User;
import com.trident.calypso.user.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/passage")
public class PassageResource {

    private final PassageService passageService;
    private final PassageRepository passageRepository;

    private final UserService userService;

    public PassageResource(
            PassageService passageService,
            PassageRepository passageRepository,
            UserService userService
    ){
        this.passageService = passageService;
        this.passageRepository = passageRepository;
        this.userService = userService;

    }

    @PostMapping("/{username}")
    public HttpStatus writeToLogBook(@PathVariable String username, @RequestBody Passage passage) {
        passage.setActive(true);
        System.out.println("Result of active: " + passage.isActive());
        passageService.writePassage(
                username,
                passage.getTitle(),
                passage.getDescription(),
                passage.getDepartureDate(),
                passage.getDepartureLocation(),
                passage.getExpenses(),
                passage.getExpectations(),
                passage.getDestination(),
                passage.isActive()
        );
        return HttpStatus.CREATED; // Return appropriate HTTP status code
    }

    @GetMapping("all/{username}")
    public List<Passage> getByUsername(String username){
        User user = userService.findUserByUsername(username);
        return passageService.getAllByUserId(user.getId());
    }

    @PostMapping("/update/{username}/{passageID}")
    public HttpStatus editPassage(@PathVariable String username,@PathVariable Long passageID,  @RequestBody Passage passage){
        System.out.println(username);
        System.out.println(passageID);
        System.out.println(passage.isActive());
        passageService.editPassage(
                username,
                passageID,
                passage.getTitle(),
                passage.getDescription(),
                passage.getDepartureDate(),
                passage.getDepartureLocation(),
                passage.getExpenses(),
                passage.getExpectations(),
                passage.getDestination(),
                passage.isActive()
        );
        return HttpStatus.OK;
    }

    @GetMapping("/all")
    public List<Passage> getAllPassages() {
        return passageService.getAllPassages();
    }

    @GetMapping("/active")
    public List<Passage> getAllActive() {
        return passageService.getAllActivePassages();
    }

    // update passage
    @PostMapping("/update/{passageID}")
    public Passage updatePassage(@PathVariable("passageID")String passageID){
        Passage selectedPassage = passageRepository.getById(Long.parseLong(passageID));
        // create service to update passage
        return selectedPassage;
    }

    // complete passage switch active to false
    @GetMapping("/complete/{username}/{passageID}")
    public HttpStatus completePassage(@PathVariable String username,@PathVariable Long passageID) throws IOException{
        Passage optionalPassageReturn = passageService.completePassage(username, passageID);
        System.out.println("Passage Complete; Active Status: " + optionalPassageReturn.isActive());
        return(HttpStatus.OK);
    }

    // delete passage
    @DeleteMapping("/delete/{username}/{passageID}")
    @PreAuthorize("hasAnyAuthority('user:delete')") // find a protective way to only allow the user to delete own passages
    public HttpStatus deletePassage(@PathVariable String username,@PathVariable Long passageID) throws IOException {
        passageService.deletePassage(username, passageID);
        return(HttpStatus.OK);
    }

}

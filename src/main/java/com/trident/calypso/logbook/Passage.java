package com.trident.calypso.logbook;

import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity
@Table(name = "passage")
public class Passage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    @ManyToOne
    private LogBook captainsLogBook;

    @Column(nullable = false)
    private LocalDateTime timestamp;


    private String title;

    @Column(length = 1000)
    private String description;

    private String departureDate;

    private String departureLocation;

    private String expenses;

    @Column(length = 1000)
    private String expectations;

    private String destination;

    private boolean isActive;

    public Passage(){};

    public Passage(Long id, LogBook captainsLogBook, LocalDateTime timestamp, String title, String description, String departureDate, String departureLocation, String expenses, String expectations, String destination, boolean isActive) {
        this.id = id;
        this.captainsLogBook = captainsLogBook;
        this.timestamp = timestamp;
        this.title = title;
        this.description = description;
        this.departureDate = departureDate;
        this.departureLocation = departureLocation;
        this.expenses = expenses;
        this.expectations = expectations;
        this.destination = destination;
        this.isActive = isActive;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LogBook getCaptainsLogBook() {
        return captainsLogBook;
    }

    public void setCaptainsLogBook(LogBook captainsLogBook) {
        this.captainsLogBook = captainsLogBook;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureLocation() {
        return departureLocation;
    }

    public void setDepartureLocation(String departureLocation) {
        this.departureLocation = departureLocation;
    }

    public String getExpenses() {
        return expenses;
    }

    public void setExpenses(String expenses) {
        this.expenses = expenses;
    }

    public String getExpectations() {
        return expectations;
    }

    public void setExpectations(String expectations) {
        this.expectations = expectations;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}

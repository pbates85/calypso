package com.trident.calypso.logbook.implementation;

import com.trident.calypso.logbook.*;
import com.trident.calypso.user.UserRepository;
import com.trident.calypso.user.User;
import java.util.NoSuchElementException;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PassageServiceImpl implements PassageService {

    private final UserRepository userRepository;
    private final LogBookRepository logBookRepository;

    private final PassageRepository passageRepository;

    private final JdbcTemplate jdbcTemplate;



    public PassageServiceImpl(LogBookRepository logBookRepository, UserRepository userRepository, PassageRepository passageRepository, DataSource dataSource){
        this.userRepository = userRepository;
        this.logBookRepository = logBookRepository;
        this.passageRepository = passageRepository;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public void writePassage(String username, String title, String description, String departureDate, String departureLocation, String expenses, String expectations, String destination, boolean isActive) {
        User user = userRepository.findUserByUsername(username);
        LogBook logBook = logBookRepository.findByOwner(user);

        if (logBook == null){
            logBook = new LogBook();
            logBook.setOwner(user);
            logBookRepository.save(logBook);
        }

        // Check if the user already has an active passage
        Passage activePassage = logBook.getPostedPassages().stream()
                .filter(Passage::isActive)
                .findFirst()
                .orElse(null);

        if (isActive && activePassage != null) {
            // Reject the creation of a new active passage
            throw new RuntimeException("User already has an active passage");
        }

        Passage passage = new Passage();
        passage.setTimestamp(LocalDateTime.now());
        passage.setTitle(title);
        passage.setDescription(description);
        passage.setDepartureDate(departureDate);
        passage.setDepartureLocation(departureLocation);
        passage.setExpenses(expenses);
        passage.setExpectations(expectations);
        passage.setDestination(destination);
        passage.setCaptainsLogBook(logBook);
        passage.setActive(isActive);

        logBook.getPostedPassages().add(passage);
        logBookRepository.save(logBook);

    }

    @Override
    public void editPassage(String username, Long passageID, String title, String description, String departureDate, String departureLocation, String expenses, String expectations, String destination, boolean isActive) {
        User user = userRepository.findUserByUsername(username);
        Passage passage = passageRepository.getById(passageID);
        LogBook logBook = logBookRepository.findByOwner(user);

        passage.setTimestamp(LocalDateTime.now());
        passage.setTitle(title);
        passage.setDescription(description);
        passage.setDepartureDate(departureDate);
        passage.setDepartureLocation(departureLocation);
        passage.setExpenses(expenses);
        passage.setExpectations(expectations);
        passage.setDestination(destination);
        passage.setCaptainsLogBook(logBook);
        passage.setActive(isActive);
        passageRepository.save(passage);
    }

    @Override
    public void deletePassage(String username, Long passageID) {
        // need to do some kind of check to make sure the submitted was done by the user
        Passage passage = passageRepository.getById(passageID);
        passageRepository.deleteById(passage.getId());

    }


    @Override
    public List<Passage> getAllPassages() {
        return passageRepository.findAll();
    }

    @Override
    public Passage completePassage(String username, Long passageID) throws IOException {
        Passage currentPassage = passageRepository.findById(passageID)
                .orElseThrow(NoSuchElementException::new); // Using the generic exception
        currentPassage.setActive(false);
        passageRepository.save(currentPassage);
        return currentPassage;
    }

    @Override
    public List<Passage> getAllActivePassages() {
        List<Passage> allPassages = passageRepository.findAll();
        return allPassages.stream()
                .filter(Passage::isActive) // Filter passages with isActive = true
                .collect(Collectors.toList());
    }

    @Override
    public List<Passage> getAllByUserId(Long userId) {
        return null;
    }

}

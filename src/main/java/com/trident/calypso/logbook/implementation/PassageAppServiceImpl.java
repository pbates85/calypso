package com.trident.calypso.logbook.implementation;

import com.trident.calypso.logbook.*;
import com.trident.calypso.user.User;
import com.trident.calypso.user.UserRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class PassageAppServiceImpl implements PassageAppService {

    private final UserRepository userRepository;
    private final PassageRepository passageRepository;
    private final PassageAppRepository passageAppRepository;

    PassageAppServiceImpl(
            UserRepository userRepository, PassageRepository passageRepository,
            PassageAppRepository passageAppRepository
    ){
        this.userRepository = userRepository;
        this.passageRepository = passageRepository;
        this.passageAppRepository =passageAppRepository;
    }

    @Override
    public void apply(String username, Long passageId, String communication, String certifications, String experience, String availableTime, String message) {
        System.out.println("Passage ID being submitted: " + passageId);
        PassageApp passageApp = new PassageApp();

        // get passage username from passageID
        Passage passage = passageRepository.getById(passageId);
        passageApp.setUsername(passage.getCaptainsLogBook().getOwner().getUsername());

        passageApp.setApplicantName(username);
        passageApp.setPassageID(passageId);
        passageApp.setCommunication(communication);
        passageApp.setCertifications(certifications);
        passageApp.setExperience(experience);
        passageApp.setAvailableTime(availableTime);
        passageApp.setMessage(message);
        passageAppRepository.save(passageApp);
    }

    @Override
    public List<PassageApp> getAllApps(Long passageID) {
        return null;
    }

    @Override
    public List<PassageApp> getAllAppsByUser(String username) {

        return passageAppRepository.findAllByUsername(username);
    }


}

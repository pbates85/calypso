package com.trident.calypso.logbook.implementation;

import com.trident.calypso.logbook.LogBook;
import com.trident.calypso.logbook.LogBookRepository;
import com.trident.calypso.logbook.LogBookService;
import com.trident.calypso.logbook.Passage;
import com.trident.calypso.user.User;
import com.trident.calypso.user.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogBookServiceImpl implements LogBookService {

    private final UserRepository userRepository;
    private final LogBookRepository logBookRepository;

    public LogBookServiceImpl(UserRepository userRepository, LogBookRepository logBookRepository){
        this.userRepository = userRepository;
        this.logBookRepository = logBookRepository;
    }
    @Override
    public List<Passage> getPassagesByUser(String username) {
        LogBook logBook = findUserLogBook(username);
        if (logBook == null) {
            logBook = new LogBook(); // Create a new LogBook object
            // You might want to set some initial properties or perform additional setup here
        }
        List<Passage> passages = logBook.getPostedPassages();
        return passages;
    }

    private LogBook findUserLogBook(String username){
        User user = userRepository.findUserByUsername(username);
        return logBookRepository.findByOwner(user);
    }
}

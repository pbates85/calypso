package com.trident.calypso.logbook;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PassageAppRepository extends JpaRepository<PassageApp, Long> {
    List<PassageApp> findAllByUsername(String username);
}

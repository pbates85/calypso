package com.trident.calypso.yacht;

import org.springframework.data.jpa.repository.JpaRepository;

public interface YachtRepository extends JpaRepository<Yacht, Long> {
    Yacht findByName(String name);

    Yacht findByMmsi(Long mmsi);

    boolean existsByMmsi(Long mmsi);

    Yacht findYachtById(Long id);
}

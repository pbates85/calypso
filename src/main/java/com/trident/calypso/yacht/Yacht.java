package com.trident.calypso.yacht;


import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;

@Entity
@Table(name = "yacht", uniqueConstraints = {@UniqueConstraint(columnNames = {"id"})})
public class Yacht {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "mmsi", nullable = false)
    private Long mmsi;

    @Column(name = "lat", nullable = true)
    private double LAT;

    @Column(name = "lon", nullable = true)
    private double LON;

    @Column(name = "port", nullable = true)
    private String port;

    @Column(name = "model", nullable = true)
    private String model;

    @Column(name = "make", nullable = true)
    private String make;

    @Column(name = "length", nullable = true)
    private Integer length;

    @Column(name = "available_cabins", nullable = true)
    private Integer availableCabins;

    @Column(name = "yachtImageUrl", nullable = true)
    private String yachtImageUrl;

    public Yacht() {}

    public Yacht(Long id, String name, Long mmsi, double LAT, double LON, String port, String model, String make, Integer length, Integer availableCabins, String yachtImageUrl) {
        this.id = id;
        this.name = name;
        this.mmsi = mmsi;
        this.LAT = LAT;
        this.LON = LON;
        this.port = port;
        this.model = model;
        this.make = make;
        this.length = length;
        this.availableCabins = availableCabins;
        this.yachtImageUrl = yachtImageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMmsi() {
        return mmsi;
    }

    public void setMmsi(Long mmsi) {
        this.mmsi = mmsi;
    }

    public double getLAT() {
        return LAT;
    }

    public void setLAT(double LAT) {
        this.LAT = LAT;
    }

    public double getLON() {
        return LON;
    }

    public void setLON(double LON) {
        this.LON = LON;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getAvailableCabins() {
        return availableCabins;
    }

    public void setAvailableCabins(Integer availableCabins) {
        this.availableCabins = availableCabins;
    }

    public String getYachtImageUrl() {
        return yachtImageUrl;
    }

    public void setYachtImageUrl(String yachtImageUrl) {
        this.yachtImageUrl = yachtImageUrl;
    }
}

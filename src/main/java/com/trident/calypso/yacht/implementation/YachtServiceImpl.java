package com.trident.calypso.yacht.implementation;

import com.trident.calypso.exception.domain.EmailExistException;
import com.trident.calypso.exception.domain.NotAnImageFileException;
import com.trident.calypso.exception.domain.UserNotFoundException;
import com.trident.calypso.exception.domain.UsernameExistException;
import com.trident.calypso.user.User;
import com.trident.calypso.user.UserRepository;
import com.trident.calypso.yacht.Yacht;
import com.trident.calypso.yacht.YachtRepository;
import com.trident.calypso.yacht.YachtService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static com.trident.calypso.contstant.FileConstant.*;
import static com.trident.calypso.contstant.FileConstant.FILE_SAVED_IN_FILE_SYSTEM;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static org.springframework.http.MediaType.*;

@Service
@Transactional
public class YachtServiceImpl implements YachtService {

    private final YachtRepository yachtRepository;
    private final UserRepository userRepository;
    private Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    public YachtServiceImpl(YachtRepository yachtRepository, UserRepository userRepository){
        this.yachtRepository = yachtRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Yacht register(String name, Long mmsi, String port, String model, String make, Integer length, Integer availableCabins, double LAT, double LON, String yachtImageUrl) {
        Yacht yacht = new Yacht();
        yacht.setName(name);
        yacht.setMmsi(mmsi);
        yacht.setPort(port);
        yacht.setModel(model);
        yacht.setMake(make);
        yacht.setLength(length);
        yacht.setAvailableCabins(availableCabins);
        yacht.setLAT(LAT);
        yacht.setLON(LON);

        // Check if yachtImageUrl is empty or null, use default image URL if true
        if (yachtImageUrl == null || yachtImageUrl.isEmpty()) {
            // angular front end will display default images
            yacht.setYachtImageUrl("/assets/img/default-sailboat.jpg");
        } else {
            yacht.setYachtImageUrl("/api/yacht/image/" + yachtImageUrl);
        }

        yachtRepository.save(yacht);
        return yacht;
    }

    @Override
    public List<Yacht> getYachts() {
        return yachtRepository.findAll();
    }

    @Override
    public Yacht findYachtById(Long id) {
        return yachtRepository.findYachtById(id);
    }

    @Override
    public Yacht findYachtByName(String name) {
        return yachtRepository.findByName(name);
    }

    @Override
    public Yacht findYachtByMmsi(Long mmsi) {
        return yachtRepository.findByMmsi(mmsi);
    }

    // Need to check for unique mmsi id's
    @Override
    public Boolean checkMmsiUnique(Long mmsi){
        return yachtRepository.existsByMmsi(mmsi);
    }

    @Override
    public Yacht update(Yacht existingYacht) {
        // Perform any necessary validation or business logic before updating the yacht
        return yachtRepository.save(existingYacht);
    }

    @Override
    public Yacht updateYachtImage(String stringMmsi, MultipartFile yachtImage) throws IOException, NotAnImageFileException {
        Long mmsi = Long.parseLong(stringMmsi);
        Yacht yacht = findYachtByMmsi(mmsi);
        saveYachtImage(yacht, yachtImage);
        return yacht;
    }

    private void saveYachtImage(Yacht yacht, MultipartFile yachtImage) throws IOException, NotAnImageFileException {
        if (yachtImage != null) {
            // check image properties ensure only images can be saved
            if(!Arrays.asList(IMAGE_JPEG_VALUE, IMAGE_PNG_VALUE, IMAGE_GIF_VALUE).contains(yachtImage.getContentType())) {
                throw new NotAnImageFileException(yachtImage.getOriginalFilename() + NOT_AN_IMAGE_FILE);
            }
            Path yachtFolder = Paths.get(YACHT_FOLDER + yacht.getMmsi().toString()).toAbsolutePath().normalize();
            // create directory
            if(!Files.exists(yachtFolder)) {
                Files.createDirectories(yachtFolder);
                LOGGER.info(DIRECTORY_CREATED + yachtFolder);
            }
            // delete if anything, that is in the folder
            Files.deleteIfExists(Paths.get(yachtFolder + yacht.getMmsi().toString() + DOT + JPG_EXTENSION));
            // add file
            Files.copy(yachtImage.getInputStream(), yachtFolder.resolve(yacht.getMmsi().toString() + DOT + JPG_EXTENSION), REPLACE_EXISTING);
            yacht.setYachtImageUrl(setYachtImageUrl(yacht.getMmsi().toString()));
            yachtRepository.save(yacht);
            LOGGER.info(FILE_SAVED_IN_FILE_SYSTEM + yachtImage.getOriginalFilename());
        }
    }

    private String setYachtImageUrl(String mmsi) {
        return ServletUriComponentsBuilder.fromCurrentContextPath().path(YACHT_IMAGE_PATH + mmsi + FORWARD_SLASH
                + mmsi + DOT + JPG_EXTENSION).toUriString();
    }
}

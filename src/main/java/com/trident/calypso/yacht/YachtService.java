package com.trident.calypso.yacht;

import com.trident.calypso.exception.domain.NotAnImageFileException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface YachtService {

    Yacht register(String name, Long mmsi, String port, String brand, String make, Integer length, Integer availableCabins, double LAT, double LON, String yachtImageUrl);

    List<Yacht> getYachts();

    Yacht findYachtById(Long id);

    Yacht findYachtByName(String name);

    Yacht findYachtByMmsi(Long mmsi);

    Boolean checkMmsiUnique(Long mmsi);

    Yacht update(Yacht existingYacht);

    Yacht updateYachtImage(String mmsi, MultipartFile yachtImage) throws IOException, NotAnImageFileException;
}

package com.trident.calypso.yacht;

import com.trident.calypso.exception.domain.EmailExistException;
import com.trident.calypso.exception.domain.NotAnImageFileException;
import com.trident.calypso.exception.domain.UserNotFoundException;
import com.trident.calypso.exception.domain.UsernameExistException;
import com.trident.calypso.user.User;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static com.trident.calypso.contstant.FileConstant.*;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.IMAGE_JPEG_VALUE;
import static org.springframework.http.MediaType.IMAGE_PNG_VALUE;

@RestController
@RequestMapping(path = { "/api/yacht"})
public class YachtResource {
    private YachtService yachtService;

    public YachtResource(YachtService yachtService){
        this.yachtService = yachtService;
    }

    @PostMapping("/register")
    public ResponseEntity<Yacht> registerYacht(
            @RequestBody Yacht yacht
    ){
        // Check if MMSI is unique
        if (yachtService.checkMmsiUnique(yacht.getMmsi())) {
            throw new DuplicateMmsiException("Duplicate MMSI found: " + yacht.getMmsi());
        }

        Yacht newYacht = yachtService.register(
                yacht.getName(),
                yacht.getMmsi(),
                yacht.getPort(),
                yacht.getModel(),
                yacht.getMake(),
                yacht.getLength(),
                yacht.getAvailableCabins(),
                yacht.getLAT(),
                yacht.getLON(),
                yacht.getYachtImageUrl()
        );
        return new ResponseEntity<>(newYacht, OK);
    }

    @PostMapping("/update")
    public ResponseEntity<Yacht> updateYacht(@RequestBody Yacht yacht) {

        Yacht existingYacht = yachtService.findYachtByMmsi(yacht.getMmsi());

        System.out.println(yacht.getName() +" " + yacht.getMmsi());

        if (existingYacht == null) {
            throw new YachtNotFoundException("Yacht not found with id: " + yacht.getMmsi());
        }

        // Update the fields if they are provided
        if (yacht.getName() != null) {
            existingYacht.setName(yacht.getName());
        }
        if (yacht.getMmsi() != null) {
            Long mmsi = yacht.getMmsi();
            // Check if MMSI is unique, excluding the current yacht
            if (!yachtService.checkMmsiUnique(mmsi)) {
                throw new DuplicateMmsiException("Duplicate MMSI found: " + mmsi);
            }
            existingYacht.setMmsi(mmsi);
        }
        if (yacht.getPort() != null) {
            existingYacht.setPort(yacht.getPort());
        }
        if (yacht.getModel() != null) {
            existingYacht.setModel(yacht.getModel());
        }
        if (yacht.getMake() != null) {
            existingYacht.setMake(yacht.getMake());
        }
        if (yacht.getLength() != null) {
            existingYacht.setLength(yacht.getLength());
        }
        if (yacht.getAvailableCabins() != null) {
            existingYacht.setAvailableCabins(yacht.getAvailableCabins());
        }

        Yacht updatedYacht = yachtService.update(existingYacht);

        return new ResponseEntity<>(updatedYacht, OK);
    }

    @GetMapping("/list")
    public ResponseEntity<List<Yacht>> getAllYachts(){
        List<Yacht> yachts = yachtService.getYachts();

        return new ResponseEntity<>(yachts, OK);
    }

    @GetMapping("/findbyid/{mmsi}")
    public ResponseEntity<Yacht> getYachtByMmsi(@PathVariable("mmsi") Long mmsi){
        Yacht yacht = yachtService.findYachtByMmsi(mmsi);
        return new ResponseEntity<>(yacht, OK);
    }

    public class DuplicateMmsiException extends RuntimeException {
        public DuplicateMmsiException(String message) {
            super(message);
        }
    }

    public class YachtNotFoundException extends RuntimeException {
        public YachtNotFoundException(String message) {
            super(message);
        }
    }

    @PostMapping("/updateYachtImage")
    public ResponseEntity<Yacht> updateProfileImage(
            @RequestParam("mmsi") String mmsi,
            @RequestParam(value = "yachtImage") MultipartFile yachtImage) throws  IOException, NotAnImageFileException {
        Yacht yacht = yachtService.updateYachtImage(mmsi, yachtImage);
        return new ResponseEntity<>(yacht, OK);
    }

    @GetMapping(path = "/image/{mmsi}/{fileName}", produces = IMAGE_JPEG_VALUE)
    public byte[] getYachtImage(@PathVariable("mmsi") String mmsi, @PathVariable("fileName") String fileName) throws IOException {
        return Files.readAllBytes(Paths.get(YACHT_FOLDER + mmsi + FORWARD_SLASH + fileName));
    }

    @GetMapping(path = "/image/profile/sail-silhouette.jpg", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getTempProfileImage() throws IOException {
        ClassPathResource resource = new ClassPathResource("static/sail-silhouette.jpg");
        InputStream inputStream = resource.getInputStream();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        int bytesRead;
        byte[] chunk = new byte[1024];
        while ((bytesRead = inputStream.read(chunk)) > 0) {
            byteArrayOutputStream.write(chunk, 0, bytesRead);
        }

        return byteArrayOutputStream.toByteArray();
    }



}

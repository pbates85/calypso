import {PassageDTO} from "./PassageDTO";
import {User} from "./user";

export class LogBookDTO {
  id?: number;
  owner: User;
  postedPassages: PassageDTO[];

  constructor(id: number, owner: User, postedPassages: PassageDTO[]) {
    this.id = id;
    this.owner = owner;
    this.postedPassages = postedPassages;
  }
}

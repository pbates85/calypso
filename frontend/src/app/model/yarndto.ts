export interface Yarndto {
  sender: string;
  recipient: string;
  message: string;
  timestamp?: string;
}

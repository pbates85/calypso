
export class PassageApp{

  communication: string;

  certifications: string;

  experience: string;

  availableTime: string;

  message: string;


  constructor() {
    this.communication = '';
    this.certifications = '';
    this.experience = '';
    this.availableTime = '';
    this.message = '';
  }
}



export class Application{

  username: string;

  applicantName: string;

  passageId: string;

  communication: string;

  certifications: string;

  experience: string;

  availableTime: string;

  message: string;


  constructor() {
    this.username = '';
    this.applicantName = '';
    this.passageId = '';
    this.communication = '';
    this.certifications = '';
    this.experience = '';
    this.availableTime = '';
    this.message = '';
  }
}

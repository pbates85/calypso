export class Yacht {
  public yachtId: string;
  public name: string;
  public mmsi: string;
  public port: string;
  public model: string;
  public make: string;
  public length: string;
  public availableCabins: string;
  public LAT: string;
  public LON: string;
  public yachtImageUrl: string;

  constructor() {
    this.yachtId = '';
    this.name = '';
    this.mmsi = '';
    this.port = '';
    this.model = '';
    this.make = '';
    this.length = '';
    this.availableCabins = '';
    this.LAT = '';
    this.LON = '';
    this.yachtImageUrl = '';
  }
}

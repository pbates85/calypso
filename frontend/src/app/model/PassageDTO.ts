import {SafeUrl} from "@angular/platform-browser";
import {LogBookDTO} from "./LogBookDTO";

export class PassageDTO {
  id: string;
  public logBook!: LogBookDTO;
  imageUrl: SafeUrl;
  timestamp: Date;
  title: string;
  description: string;
  departureDate: string;
  departureLocation: string;
  expenses: string;
  expectations: string;
  destination: string;
  active: boolean;

  // Constructor with default argument values
  constructor() {
    this.id = '';
    this.imageUrl = '';
    this.timestamp = null!;
    this.title = '';
    this.description = '';
    this.departureDate = '';
    this.departureLocation = '';
    this.expenses = '';
    this.expectations = '';
    this.destination = '';
    this.active = false;
  }
}


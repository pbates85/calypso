export interface Postdto {
  sender: string;
  recipient: string;
  content: string;
}

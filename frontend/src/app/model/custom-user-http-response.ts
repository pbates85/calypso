export interface CustomUserHttpResponse {
  httpStatusCode: number;
  httpStatus: string;
  reason: string;
  message: string;
}

import {Yacht} from "./yacht";

export class Captain {
  public id?: number;
  public firstName: string;
  public lastName: string;
  public email: string;
  public phoneNumber: string;
  public yacht!: Yacht;

  constructor() {
    this.firstName = '';
    this.lastName = '';
    this.email = '';
    this.phoneNumber = '';
  }
}


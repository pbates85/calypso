import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {AuthenticationGuard} from "./guard/authentication.guard";
import {UserComponent} from "./user/user.component";
import {FrontpageComponent} from "./frontpage/frontpage.component";
import {CaptainComponent} from "./captain/captain.component";
import {PassagesComponent} from "./passages/passages.component";
const routes: Routes = [
  { path: '', component: FrontpageComponent},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'user/management', component: UserComponent, canActivate: [AuthenticationGuard] },
  { path: 'captain', component: CaptainComponent, canActivate: [AuthenticationGuard] },
  { path: 'passages', component: PassagesComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Captain } from '../model/captain';


@Injectable({
  providedIn: 'root'
})

export class CaptainService {

  mmsi!: number;

  private host = environment.apiUrl;
  constructor(private http: HttpClient) {}

  public addCaptain(captain: Captain): Observable<Captain> {
    return this.http.post<Captain>(`${this.host}/captain/register`, captain);
  }

  public registerCaptain(email: string, mmsi: string): Observable<Captain> {
    return this.http.post<Captain>(`${this.host}/captain/attach-yacht`, { email, mmsi });
  }

  public createCaptainFormData(captain: Captain): any {
    const formData: any = {
      firstName: captain.firstName,
      lastName: captain.lastName,
      email: captain.email,
      phoneNumber: captain.phoneNumber,
      mmsi: this.mmsi
    };


    return formData;
  }

  public getCaptains(): Observable<Captain[]>{
    return this.http.get<Captain[]>(`${this.host}/captain/list`);
  }

  public getCaptainById(email: string): Observable<Captain> {
    return this.http.get<Captain>(`${this.host}/captain/email/${email}`);
  }

  public getCaptainByUserName(username: string): Observable<Captain> {
    return this.http.get<Captain>(`${this.host}/captain/user/${username}`);
  }

}

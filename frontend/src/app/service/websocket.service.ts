import { Injectable } from '@angular/core';
import { Client, Frame, Message } from '@stomp/stompjs';
import {BehaviorSubject, Observable} from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  private stompClient: Client;
  private connectedSubject = new BehaviorSubject<boolean>(false);
  public connected$: Observable<boolean> = this.connectedSubject.asObservable();
  public posts: string[] = [];


  constructor() {
    this.stompClient = new Client();
    this.stompClient.brokerURL = 'ws://localhost:8080/ws';
  }

  public connect(): void {
    this.stompClient.onConnect = (frame: Frame) => {
      this.setConnected(true);
      console.log('Connected: ' + frame);
      this.stompClient.subscribe('/topic/public', (post: Message) => {
        this.showPosts(JSON.parse(post.body).content);
      });
    };

    this.stompClient.onWebSocketError = (error: Error) => {
      console.error('Error with WebSocket', error);
    };

    this.stompClient.onStompError = (frame: Frame) => {
      console.error('Broker reported error: ' + frame.headers['message']);
      console.error('Additional details: ' + frame.body);
    };

    this.stompClient.activate();
  }

  public disconnect(): void {
    if (this.stompClient.connected) {
      this.stompClient.onDisconnect = () => {
        this.setConnected(false);
        console.log('Disconnected');
      };
      this.stompClient.deactivate();
    }
  }

  public sendPost(post: string): void {
    this.stompClient.publish({
      destination: '/app/public',
      body: JSON.stringify({ content: post})
    });
  }

  private setConnected(connected: boolean): void {
    this.connectedSubject.next(connected);
  }

  private showPosts(post: string): void {
    this.posts.push(post);
  }

}

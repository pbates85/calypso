import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({providedIn: 'root'})
export class MessageService {

  constructor(
    private http: HttpClient
  ) {}

  sendMessage(message: string) {
    // K8s cluster addr http://chat-bot-service.staging:5000, local testing use http://localhost:5000/message
    return this.http.post('https://calypso.phillipbates.com/message', {prompt: message})
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from "../../environments/environment";
import {Yarndto} from "../model/yarndto";
import {Postdto} from "../model/postDto";



@Injectable({
  providedIn: 'root'
})
export class YarnMessageService {
  private host = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public createYarnFormData(yarn: Postdto): any {
    const formData: any = {
      sender: yarn.sender,
      recipient: yarn.recipient,
      message: yarn.content
    };
    return formData;
  }

  public getMessages(username: string): Observable<Yarndto[]> {
    return this.http.get<Yarndto[]>(`${this.host}/yarn/messages/${username}`);
  };

  public postYarn(yarnPostDto: Postdto): Observable<Postdto> {
    console.log('Post hit')
    return this.http.post<Postdto>(`${this.host}/yarn/write`, yarnPostDto);
  };
}

import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {PassageDTO} from "../model/PassageDTO";
import {PassageApp} from "../model/passageapp";
import {Application} from "../model/application";

@Injectable({
  providedIn: 'root'
})
export class PassagesService {
  private host = environment.apiUrl;
  constructor(private http: HttpClient) { }

  //grab all active passages
  public getAllActivePassages(): Observable<PassageDTO[]> {
    return this.http.get<PassageDTO[]>(`${this.host}/passage/active`);
  }

  public getAllUsersPassages(username: string): Observable<PassageDTO[]> {
    return this.http.get<PassageDTO[]>(`${this.host}/logbook/passages/${username}`);
  }

  public getAllUsersApplications(username: string): Observable<Application[]> {
    return this.http.get<Application[]>(`${this.host}/applications/user/${username}`);
  }

  public addPassagesToLocalCache(passages: PassageDTO[]): void {
    localStorage.setItem('activePassages', JSON.stringify(passages));
  }

  public getActivePassagesFromLocalCache(): PassageDTO[] {
    if (localStorage.getItem('activePassages')) {
      return JSON.parse(localStorage.getItem('activePassages')!);
    }
    return null!;
  }

  public createPassageFormData(passage: PassageDTO): any {
    const passageFormData: any = {
      title: passage.title,
      description: passage.description,
      departureDate: passage.departureDate,
      departureLocation: passage.departureLocation,
      expenses: passage.expenses,
      expectations: passage.expectations,
      destination: passage.destination,
      active: passage.active
    };
    return passageFormData;
  }

  public postPassage(username: string,passage: PassageDTO): Observable<HttpEvent<any>> {
    return this.http.post<HttpEvent<any>>(`${this.host}/passage/${username}`, passage);
  }

  public applyPassage(username: string, passageid: string, application: PassageApp): Observable<HttpEvent<any>> {
    return this.http.post<HttpEvent<any>>(`${this.host}/applications/${username}/${passageid}`, application);
  }

  public updatePassage(username: string, passageid: string, formData: any): Observable<HttpEvent<any>> {
    console.log("Sending Data: ", username, passageid, formData)
    return this.http.post<HttpEvent<any>>(`${this.host}/passage/update/${username}/${passageid}`, formData);
  }

  public completePassage(username: string, passageid: string): Observable<HttpEvent<any>> {
    console.log("Sending Data: ", username, passageid)
    return this.http.get<HttpEvent<any>>(`${this.host}/passage/complete/${username}/${passageid}`);
  }
}

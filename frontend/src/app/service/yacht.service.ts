import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient, HttpEvent} from "@angular/common/http";
import {Observable} from "rxjs";
import {Yacht} from "../model/yacht";
import {User} from "../model/user";

@Injectable({
  providedIn: 'root'
})
export class YachtService {
  private host = environment.apiUrl;
  private apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) {}


  public getYachts(): Observable<Yacht[]>{
    return this.http.get<Yacht[]>(`${this.host}/yacht/list`);
  }
  public getYachtById(mmsi: number): Observable<Yacht>{
    return this.http.get<Yacht>(`${this.host}/yacht/findbyid/${mmsi}`);
  }

  public addYachtsToLocalCache(yachts: Yacht[]): void {
    localStorage.setItem('yachts', JSON.stringify(yachts));
  }

  public addYachtToLocalCache(yacht: Yacht): void {
    // local storage cannot take an object only strings
    localStorage.setItem('yacht', JSON.stringify(yacht));
  }

  public getYachtsFromLocalCache(): Yacht[] {
    if (localStorage.getItem('yachts')) {
      return JSON.parse(localStorage.getItem('yachts')!);
    }
    return null!;
  }

  public addYacht(yacht: Yacht): Observable<Yacht> {
    return this.http.post<Yacht>(`${this.host}/yacht/register`, yacht);
  }

  createYachtFormData(yacht: Yacht): any {
    const yachtFormData: any = {
      name: yacht.name,
      mmsi: yacht.mmsi,
      port: yacht.port,
      model: yacht.model,
      make: yacht.make,
      length: yacht.length,
      availableCabins: yacht.availableCabins
    };
    return yachtFormData;
  }

  public updateYachtImage(formData: FormData): Observable<HttpEvent<Yacht>> {
    return this.http.post<Yacht>(`${this.host}/yacht/updateYachtImage`, formData,
      {reportProgress: true,
        observe: 'events'
      });
  }

  getImageUrl(yachtImageUrl?: string): string {
    if (yachtImageUrl) {
      if (environment.production) {
        // Assuming your backend endpoint for yacht images is /api/yacht/image/
        return `${this.apiUrl}/yacht/${yachtImageUrl}`;
      } else {
        // In development mode, use the relative path directly
        return `${this.apiUrl}/yacht${yachtImageUrl}`;
      }
    } else {
      // If no yachtImageUrl is provided, return a placeholder or default image URL
      // Use the correct relative path to the fallback image
      return '/assets/img/default-sailboat.jpg';
    }
  }

  public updateYacht(formData: FormData): Observable<Yacht> {
    return this.http.post<Yacht>(`${this.host}/yacht/update`, formData);
  }

}

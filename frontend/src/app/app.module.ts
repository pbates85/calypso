import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { AuthInterceptor } from './interceptor/auth.interceptor';
import {UserService} from "./service/user.service";
import {AuthenticationService} from "./service/authentication.service";
import { AuthenticationGuard } from './guard/authentication.guard';
import {NotificationModule} from "./notification.module";
import {RouterOutlet} from "@angular/router";
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {UserComponent} from "./user/user.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NotificationService} from "./service/notification.service";
import {NotifierModule} from "angular-notifier";
import { FrontpageComponent } from './frontpage/frontpage.component';
import {MessageService} from "./service/message.service";
import {CaptainComponent} from "./captain/captain.component";
import {PassagesComponent} from "./passages/passages.component";


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UserComponent,
    FrontpageComponent,
    CaptainComponent,
    PassagesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NotificationModule,
    HttpClientModule,
    RouterOutlet,
    FormsModule,
    NotifierModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [NotificationService, MessageService, AuthenticationGuard, AuthenticationService, UserService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true } ],
  bootstrap: [AppComponent]
})
export class AppModule { }

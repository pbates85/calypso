import {Component, OnDestroy, OnInit} from '@angular/core';
import {BehaviorSubject, Observable, Subscription} from "rxjs";
import {User} from "../model/user";
import {Router} from "@angular/router";
import {AuthenticationService} from "../service/authentication.service";
import {UserService} from "../service/user.service";
import {NotificationService} from "../service/notification.service";
import {PassageDTO} from "../model/PassageDTO";
import {PassagesService} from "../service/passages.service";
import {NotificationType} from "../enum/notification-type.enum";
import {HttpErrorResponse, HttpEvent} from "@angular/common/http";
import {DomSanitizer, SafeUrl} from "@angular/platform-browser";
import {NgForm} from "@angular/forms";
import {PassageApp} from "../model/passageapp";
import {Postdto} from "../model/postDto";
import {Yacht} from "../model/yacht";
import {CaptainService} from "../service/captain.service";
import {Captain} from "../model/captain";

@Component({
  selector: 'app-passages',
  templateUrl: './passages.component.html',
  styleUrls: ['./passages.component.css']
})
export class PassagesComponent implements OnInit, OnDestroy{

  private titleSubject = new BehaviorSubject<string>('Beta testing is free to use!');
  public titleAction$ = this.titleSubject.asObservable();
  private subscriptions: Subscription[] = [];
  public isProfileSelected = false;
  isApplicationModalSelected: boolean = false;
  public user!: User;
  public passages!: PassageDTO[];
  public passageDTO!: PassageDTO;
  public refreshing!: boolean;
  public selectedPassage: PassageDTO | undefined;
  public passageApp: PassageApp;
  public captain!: Captain;


  constructor(private router: Router, private authenticationService: AuthenticationService,
              private userService: UserService, private notificationService: NotificationService,
              private passageService: PassagesService, private sanitizer: DomSanitizer,
              private captainService: CaptainService
              ) {
    this.passageApp = new PassageApp();
  }

  ngOnInit(): void {
    this.passages = []; // Initialize passages as an empty array
    this.user = this.authenticationService.getUserFromLocalCache();
    // Check if the user is authenticated (assuming you have a method in AuthenticationService to check authentication)
    if (!this.authenticationService.isUserLoggedIn()) {
      // If not authenticated, redirect to the login page
      // this.router.navigate(['/login']); // Maybe a prompt to encourage register
    }
    // get passages
    this.getActivePassages(true);
    this.passageDTO = new PassageDTO();
  }

  public getActivePassages(showNotification: boolean): void {
    this.refreshing = true;
    this.subscriptions.push(
      this.passageService.getAllActivePassages().subscribe(
        (response: PassageDTO[]) => {
          console.log(response);
          this.passages = response.map(passage => ({
            ...passage,
            imageUrl: this.getRandomImageUrl(passage)
          }));
          this.refreshing = false;
          if (showNotification) {
            this.sendNotification(NotificationType.SUCCESS, `${response.length} passage(s) loaded successfully.`);
          }
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
          this.refreshing = false;
        }
      )
    );
  }

  private sendNotification(notificationType: NotificationType, message: string): void {
    if (message) {
      this.notificationService.notify(notificationType, message);
    } else {
      this.notificationService.notify(notificationType, 'An error occurred. Please try again.');
    }
  }

  public onSelectPassage(selectedPassage: PassageDTO): void {
    this.selectedPassage = selectedPassage;
    console.log('Selected Passage:', this.selectedPassage)
    // get yacht info with passage
    console.log("Captain: ", this.selectedPassage)
    this.clickButton('openPassageInfo');
  }

  onOpenApplicationModal(): void {
    this.isApplicationModalSelected = true;
    this.clickButton('openApplicationModal');
  }


  private clickButton(buttonId: string): void {
    // @ts-ignore
    document.getElementById(buttonId).click();
  }

  redirectToLogin(): void {
    if (!this.user) {
      this.router.navigate(['/login']);
    }
  }

  public changeTitle(title: string): void {
    this.titleSubject.next(title);

    if (title === 'Profile') {
      this.isProfileSelected = true;
      this.isApplicationModalSelected = false;
    } else if (title === 'Application'){
      this.isProfileSelected = false;
      this.isApplicationModalSelected = true;
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  private getRandomImageUrl(passage: PassageDTO): SafeUrl {
    const randomImageNumber = this.getRandomImageNumber();
    const imageUrl = `assets/img/passages/sailing-landscape-${randomImageNumber}.jpg`;
    passage.imageUrl = this.sanitizer.bypassSecurityTrustUrl(imageUrl);
    return passage.imageUrl;
  }


  private getRandomImageNumber(): number {
    return Math.floor(Math.random() * 20) + 1;
  }

  private setCaptainOnSelect(username: string): void {
    this.captainService.getCaptainByUserName(username)
      .subscribe(
        captain => {
          this.captain = captain;
        },
        error => {
          console.error('Error getting captain:', error);
          console.log("Error")
        }
      );
  }

  // apply to passage if signed in otherwise redirect to login
  applyPassage(appForm: NgForm): void {
    // Check if the user is signed in
    if (!this.authenticationService.isUserLoggedIn()) {
      this.redirectToLogin();
      return;
    }

    if (!this.selectedPassage) {
      // Handle the case where selectedPassage is undefined
      console.error('Selected passage is undefined.');
      return;
    }

    let app = new PassageApp(); // No need to pass the 'id' here
    app.communication = appForm.value.communication;
    app.certifications = appForm.value.certifications;
    app.experience = appForm.value.experience;
    app.availableTime = appForm.value.availableTime;
    app.message = appForm.value.message;
    console.log('Selected Passage ID:', this.selectedPassage.id);
    this.subscriptions.push(
      this.passageService.applyPassage(this.user.username, this.selectedPassage.id, app).subscribe(
        (event: HttpEvent<any>) => {
          this.sendNotification(NotificationType.SUCCESS, `Passage posted successfully`);

          // Reset the form fields
          appForm.resetForm();
          appForm.form.markAsPristine();
          appForm.form.markAsUntouched();

          // Close the modal
          this.clickButton('application-close');
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
          this.refreshing = false;
        }
      )
    );
  }




}

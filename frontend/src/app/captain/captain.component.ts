import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BehaviorSubject, Subscription} from "rxjs";
import {User} from "../model/user";
import {Captain} from "../model/captain";
import {Router} from "@angular/router";
import {AuthenticationService} from "../service/authentication.service";
import {CaptainService} from "../service/captain.service";
import {NotificationService} from "../service/notification.service";
import {NgForm} from "@angular/forms";
import {NotificationType} from "../enum/notification-type.enum";
import {HttpErrorResponse, HttpEvent, HttpEventType} from "@angular/common/http";
import {UserService} from "../service/user.service";
import {Yacht} from "../model/yacht";
import {YachtService} from "../service/yacht.service";
import {YarnMessageService} from "../service/yarn.service";
import {Yarndto} from "../model/yarndto";
import {FileUploadStatus} from "../model/file-upload.status";
import {Postdto} from "../model/postDto";
import {PassageDTO} from "../model/PassageDTO";
import {PassagesService} from "../service/passages.service";
import {PassageApp} from "../model/passageapp";
import {Application} from "../model/application";


@Component({
  selector: 'app-captain',
  templateUrl: './captain.component.html',
  styleUrls: ['./captain.component.css']
})
export class CaptainComponent implements OnInit, OnDestroy{
  private titleSubject = new BehaviorSubject<string>('Beta testing is free to use!');
  public titleAction$ = this.titleSubject.asObservable();
  public yacht: Yacht;
  public yachts!: Yacht[];
  public selectedYacht: Yacht | undefined;
  public users!: User[];
  public user!: User;
  public captain!: Captain;
  public passage!: PassageDTO;
  public captainsPassages!: PassageDTO[];
  public refreshing!: boolean;
  private subscriptions: Subscription[] = [];
  public selectedUser: User | undefined;
  public selectedCaptain: Captain | undefined;
  public mmsi: number = 0;
  public phoneNumber: string = '';
  public hasRegisteredAsCaptain = true;
  public hasYachtAttached = false;
  public isProfileSelected = false;
  public isYachtProfileSelected = false;
  public isPassageModalSelected = false;
  public isLogBookModalSelected = false;
  public isApplicationModalSelected = false;
  public isRegisteredCaptain = false;
  public exceedActivePosts = false;
  public yarnMessages: Yarndto[] = [];
  public yarnDto!: Yarndto;
  public sender!: string;
  public recipient!: string;
  public yarnPostDto!: Postdto;
  public yarn!: string;
  public reversedYarnMessages: Yarndto[] = []; // Temporary array to store the reversed messages for this modal
  public profileImage!: File;
  public yachtImage!: File;
  public fileStatus = new FileUploadStatus();
  private fileName!: string;
  refreshedOnce: boolean = false;
  // Define local properties for yacht information
  public yachtName: string = '';
  public availableCabins: string = '';
  public port: string = '';
  public makeModel: string = '';
  public length: string = '';
  public passageApp!: Application;
  public allPassageApps!: Application[];
  public areYouACaptain = true;


  constructor(private router: Router, private authenticationService: AuthenticationService,
              private userService: UserService, private notificationService: NotificationService,
              private yachtService: YachtService, private captainService: CaptainService,
              private yarnService: YarnMessageService, private passageService: PassagesService) {
    this.yacht = new Yacht();
    this.captain = new Captain();
    // Reminder to initialize an object that will have its attribute modified
    this.passage = new PassageDTO();
  }

  ngOnInit(): void {
    // call functions before the page loads
    this.user = this.authenticationService.getUserFromLocalCache();
    this.yarnMessages = [];
    this.captainsPassages = [];
    this.captain = new Captain();
    this.yacht = new Yacht();
    this.fetchYarnMessages();
    this.getAllUsersPassages(true);
    this.getAllUsersApplications(true);
    this.setCaptainAndYacht();
  }



  getCaptainFromLocalCache(): any {
    return JSON.parse(localStorage.getItem('captain')!);
  }

  getYachtFromLocalCache(): any {
    return JSON.parse(localStorage.getItem('yacht')!);
  }

  //Check if users is a captain
  private setCaptainAndYacht(): void {
    this.captainService.getCaptainById(this.user.email).subscribe(
      (captain: Captain) => {
        if(captain == null){
          this.areYouACaptain = false;
        }
        localStorage.setItem('captain', JSON.stringify(captain));
        localStorage.setItem('yacht', JSON.stringify(captain?.yacht));
        this.captain = this.getCaptainFromLocalCache();
        this.yacht = this.getYachtFromLocalCache();
      },
      (error) => {
        console.log("Registration Error")
      }
    );
  }


  private storeCaptainInLocalStorage(captain: Captain): void {
    localStorage.setItem('captain', JSON.stringify(captain));
  }

  public changeTitle(title: string): void {
    this.titleSubject.next(title);

    if (title === 'Profile') {
      this.isProfileSelected = true;
      this.isYachtProfileSelected = false;
      this.isPassageModalSelected = false;
      this.isApplicationModalSelected = false;
    } else if (title === 'Yacht Profile') {
      this.isProfileSelected = false;
      this.isYachtProfileSelected = true;
      this.isPassageModalSelected = false;
      this.isApplicationModalSelected = false;
    } else if (title === 'Log Book') {
      this.isProfileSelected = false;
      this.isYachtProfileSelected = false;
      this.isLogBookModalSelected = true;
      this.isApplicationModalSelected = false;
    } else if (title === 'Applications') {
      this.isProfileSelected = false;
      this.isYachtProfileSelected = false;
      this.isLogBookModalSelected = false;
      this.isApplicationModalSelected = true;
    }
    else {
      this.isProfileSelected = false;
      this.isYachtProfileSelected = false;
      this.isLogBookModalSelected = false;
      this.isApplicationModalSelected = false;
    }
  }

  public onSelectUser(selectedUser: User): void {
    this.selectedUser = selectedUser;
    this.fetchUserMessages(selectedUser); // Fetch the messages for the selected user
    this.clickButton('openUserInfo');
  }

  public onSelectCaptain(selectedCaptain: Captain): void {
    this.selectedCaptain = selectedCaptain;
    this.clickButton('openUserInfo');
  }

  // Click hidden save button
  public saveNewCaptain(): void {
    this.clickButton('new-captain-save');
  }

  // Click hidden save button
  public saveNewYacht(): void {
    this.clickButton('new-yacht-save');
  }

  // Click hidden save button
  public sendNewYarn(): void {
    this.clickButton('new-yarn-send');
  }

  public getAllUsersPassages(showNotification: boolean): void {
    this.refreshing = true;
    this.subscriptions.push(
      this.passageService.getAllUsersPassages(this.user.username).subscribe(
        (response: PassageDTO[]) => {
          this.captainsPassages = response.map(passage => ({
            ...passage,
          }));
          // Check if any passage has isActive set to true
          this.exceedActivePosts = this.captainsPassages.some(passage => passage.active);

          this.refreshing = false;
          if (showNotification) {
            this.sendNotification(NotificationType.SUCCESS, `Log Book loaded successfully.`);
          }
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
          this.refreshing = false;
        }
      )
    );
  }

  public getAllUsersApplications(showNotification: boolean): void {
    this.refreshing = true;
    this.subscriptions.push(
      this.passageService.getAllUsersApplications(this.user.username).subscribe(
        (response: Application[]) => {
          this.allPassageApps = response.map(passageApp => ({
            ...passageApp,
          }));
          this.refreshing = false;
          if (showNotification) {
            this.sendNotification(NotificationType.SUCCESS, `Applications loaded successfully.`);
          }
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
          this.refreshing = false;
        }
      )
    );
  }

  public postPassage(): void {
    this.clickButton('new-passage-post');
  }

  public onSubmitRegistration(registrationForm: NgForm): void {
    const yachtFormData = this.yachtService.createYachtFormData(registrationForm.value);
    yachtFormData.name = this.yacht.name;
    yachtFormData.mmsi = this.yacht.mmsi;
    yachtFormData.port = this.yacht.port;
    yachtFormData.model = this.yacht.model;
    yachtFormData.make = this.yacht.make;
    yachtFormData.length = this.yacht.length;
    yachtFormData.availableCabins = this.yacht.availableCabins;

    this.subscriptions.push(
      this.yachtService.addYacht(yachtFormData).subscribe(
        (response: Yacht) => {

          this.sendNotification(
            NotificationType.SUCCESS,
            `${response.name} added successfully as Yacht`
          );
          // Now that the yacht is created, proceed with captain registration
          const captainFormData = this.captainService.createCaptainFormData(registrationForm.value);
          captainFormData.firstName = this.user.firstName;
          captainFormData.lastName = this.user.lastName;
          captainFormData.email = this.user.email;
          captainFormData.phoneNumber = this.phoneNumber;

          this.subscriptions.push(
            this.captainService.addCaptain(captainFormData).subscribe(
              (captainResponse: Captain) => {
                this.clickButton('registrationModal');
                registrationForm.reset();
                // Store the captain object in local storage
                this.storeCaptainInLocalStorage(captainResponse);
                this.sendNotification(
                  NotificationType.SUCCESS,
                  `${captainResponse.firstName} ${captainResponse.lastName} added successfully as Captain`
                );
                if (yachtFormData.mmsi) {
                  this.subscriptions.push(
                    this.captainService.registerCaptain(captainFormData.email, yachtFormData.mmsi).subscribe(
                      () => {
                        console.log('Captain registered with MMSI successfully');
                        window.location.reload();
                      },
                      (errorResponse: HttpErrorResponse) => {
                        this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
                      }
                    )
                  );
                }
              },
              (errorResponse: HttpErrorResponse) => {
                this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
              }
            )
          );
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
        }
      )
    );
  }

  public onUpdateCurrentYacht(yacht: Yacht): void {
    this.refreshing = true;
    const formData = this.yachtService.createYachtFormData(yacht);
    formData.name = this.yacht.name;
    formData.mmsi = this.yacht.mmsi;
    formData.port = this.yacht.port;
    formData.model = this.yacht.model;
    formData.make = this.yacht.make;
    formData.length = this.yacht.length;
    formData.availableCabins = this.yacht.availableCabins;
    console.log(formData);
    this.subscriptions.push(
      this.yachtService.updateYacht(formData).subscribe(
        (response: Yacht) => {
          this.yachtService.addYachtToLocalCache(response);
          this.sendNotification(NotificationType.SUCCESS, `${response.name} updated successfully`);
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
          this.refreshing = false;
          this.profileImage = null!;
        }
      )
    );
  }

  public onSelectPassage(selectedPassage: PassageDTO): void {
    this.passage = selectedPassage;
    console.log('Selected Passage:', this.passage)
    this.isPassageModalSelected = true;
    this.clickButton('openPassageInfo');
  }

  public onSelectApplication(selectedApplication: Application): void {
    this.passageApp = selectedApplication;
    this.isApplicationModalSelected = true;
    this.clickButton('openApplicationMessageModal');
  }


  onPostPassage(passageForm: NgForm): void {
    let passageData = new PassageDTO()
    passageData.title = passageForm.value.title;
    passageData.description = passageForm.value.description;
    passageData.departureDate = passageForm.value.departureDate;
    passageData.departureLocation = passageForm.value.departureLocation;
    passageData.expenses = passageForm.value.expenses;
    passageData.expectations = passageForm.value.expectations;
    passageData.destination = passageForm.value.destination;
    this.subscriptions.push(
      this.passageService.postPassage(this.user.username, passageData).subscribe(
        (event: HttpEvent<any>) => {
          console.log("sending");
          console.log(passageData);
          this.sendNotification(NotificationType.SUCCESS, `Passage posted successfully`);
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
          this.refreshing = false;
        }
      )
    );
  }

  public getUsers(showNotification: boolean): void {
    this.refreshing = true;
    this.subscriptions.push(
      this.userService.getUsers().subscribe(
        (response: User[]) => {
          const activeUsers = response.filter(user => user.active);
          this.userService.addUsersToLocalCache(activeUsers);
          this.users = activeUsers;
          this.refreshing = false;
          if (showNotification) {
            this.sendNotification(NotificationType.SUCCESS, `${activeUsers.length} active user(s) loaded successfully.`);
          }
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
          this.refreshing = false;
        }
      )
    );
  }

  public searchUsers(searchTerm: string): void {
    const results: User[] = [];
    for (const user of this.userService.getUsersFromLocalCache()) {
      if (user.firstName.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
        user.lastName.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
        user.username.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
        user.userId.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
        results.push(user);
      }
    }
    this.users = results;
    if (results.length === 0 || !searchTerm) {
      this.users = this.userService.getUsersFromLocalCache();
    }
  }

  private clickButton(buttonId: string): void {
    const button = document.getElementById(buttonId);
    if (button) {
      button.click();
    } else {
      console.error(`Button with id ${buttonId} not found.`);
    }
  }

  public onSelectYacht(selectedYacht: Yacht): void {
    this.selectedYacht = selectedYacht;
    this.clickButton('openYachtInfo');
  }

  private sendNotification(notificationType: NotificationType, message: string): void {
    if (message) {
      this.notificationService.notify(notificationType, message);
    } else {
      this.notificationService.notify(notificationType, 'An error occurred. Please try again.');
    }
  }

  private fetchYarnMessages(): void {
    const username = this.user.username; // Assuming the user object has a `username` property
    this.yarnService.getMessages(username).subscribe(
      (response: Yarndto[]) => {
        this.yarnMessages = response;
      },
      (error: any) => {
        console.error('Failed to fetch yarn messages:', error);
      }
    );
  }

  // Function to be called when the user scrolls down
  public onScroll(): void {
    // Check if the user has reached the bottom of the container
    const container = document.querySelector('#scroll-div'); // Use the hash symbol for ID selector
    // @ts-ignore
    if (container.scrollHeight - container.scrollTop === container.clientHeight) {
      this.fetchYarnMessages();
    }
  }

  public sendYarn(newYarnForm: NgForm): void {
    // Create a YarnPostDto object and populate its properties
    const yarnPostDto: Postdto = {
      sender: this.user.username,
      recipient: this.recipient,
      content: this.yarn
    };

    this.subscriptions.push(
      this.yarnService.postYarn(yarnPostDto).subscribe(
        (response: Postdto) => {
          // Clear recipient and yarn message inputs
          this.recipient = '';
          this.yarn = '';

          // Mark the form as pristine and untouched to prevent validation errors
          newYarnForm.form.markAsPristine();
          newYarnForm.form.markAsUntouched();
          this.clickButton('yarn-close');
          this.sendNotification(
            NotificationType.SUCCESS,
            'Post sent'
          );
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
        }
      )
    );
  }

  private fetchUserMessages(selectedUser: User): void {
    this.yarnService.getMessages(selectedUser.username).subscribe(
      (response: Yarndto[]) => {
        this.reversedYarnMessages = response.slice().reverse();
      },
      (error: any) => {
        console.error('Failed to fetch user messages:', error);
      }
    );
  }

  public handlerFileInput(file: FileList): void {
    // @ts-ignore
    this.fileName = file.item(0).name;
    // @ts-ignore
    this.profileImage = file.item(0);
  }

  public handlerYachtFileInput(file: FileList): void {
    // @ts-ignore
    this.fileName = file.item(0).name;
    // @ts-ignore
    this.yachtImage = file.item(0);
  }

  public yachtHandlerFileInput(yachtFile: FileList): void {
    // @ts-ignore
    this.fileName = yachtFile.item(0).name;
    // @ts-ignore
    this.yachtImage = yachtFile.item(0);
  }

  public onUpdateProfileImage(): void {
    const formData = new FormData();
    // @ts-ignore
    formData.append('username', this.user.username);
    formData.append('profileImage', this.profileImage);
    this.subscriptions.push(
      this.userService.updateProfileImage(formData).subscribe(
        (event: HttpEvent<any>) => {
          this.reportUploadProgress(event);
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
          this.fileStatus.status = 'done';
        }
      )
    );
  }

  public onUpdateYachtImage(): void {
    const formData = new FormData();
    // @ts-ignore
    formData.append('mmsi', this.captain.yacht.mmsi.toString());
    formData.append('yachtImage', this.yachtImage);
    this.subscriptions.push(
      this.yachtService.updateYachtImage(formData).subscribe(
        async (event: HttpEvent<any>) => {
          await this.reportYachtUploadProgress(event); // Wait for the reportYachtUploadProgress to complete
          this.delayAndRefreshPage();
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
          this.fileStatus.status = 'done';
        }
      )
    );
  }

  private async delayAndRefreshPage(): Promise<void> {
    // Add a delay of 1 second before refreshing the page
    await this.delay(1000);
    // Force page refresh
    window.location.reload();
  }
  private delay(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  private async reportYachtUploadProgress(event: HttpEvent<any>): Promise<void> {
    switch (event.type) {
      case HttpEventType.UploadProgress:
        // @ts-ignore
        this.fileStatus.percentage = Math.round(100 * event.loaded / event.total);
        this.fileStatus.status = 'progress';
        break;
      case HttpEventType.Response:
        if (event.status === 200) {
          // @ts-ignore
          this.captain.yacht.yachtImageUrl = `${event.body.yachtImageUrl}?time=${new Date().getTime()}`;
          this.sendNotification(NotificationType.SUCCESS, `image updated successfully`);
          this.fileStatus.status = 'done';

          // Add a delay of 1 second before refreshing the page
          await this.delay(1000);

          // Force page refresh
          // window.location.reload();
        } else {
          this.sendNotification(NotificationType.ERROR, `Unable to upload image. Please try again`);
        }
        break; // Don't fall through to the default case
      default:
        // Do nothing for other event types
        break;
    }
  }



  private reportUploadProgress(event: HttpEvent<any>): void {
    switch (event.type) {
      case HttpEventType.UploadProgress:
        // @ts-ignore
        this.fileStatus.percentage = Math.round(100 * event.loaded / event.total);
        this.fileStatus.status = 'progress';
        break;
      case HttpEventType.Response:
        if (event.status === 200) {
          // @ts-ignore
          this.user.profileImageUrl = `${event.body.profileImageUrl}?time=${new Date().getTime()}`;
          this.sendNotification(NotificationType.SUCCESS, `${event.body.firstName}\'s profile image updated successfully`);
          this.fileStatus.status = 'done';
          break;
        } else {
          this.sendNotification(NotificationType.ERROR, `Unable to upload image. Please try again`);
          break;
        }
      default:
        `Finished all processes`;
    }
  }

  public updateProfileImage(): void {
    this.clickButton('profile-image-input');
  }

  public updateYachtImage(): void {
    this.clickButton('yacht-image-input');
  }

  onOpenLogBookModal(): void {
    this.isPassageModalSelected = true;
    this.clickButton('openLogBookModal');
  }

  onOpenCompletePassageModal(selectedPassage: PassageDTO): void {
    this.passage = selectedPassage;
    this.isPassageModalSelected = true;
    this.clickButton('openCompletePassageModal');
  }


  public onLogOut(): void {
    this.authenticationService.logOut();
    this.router.navigate(['/login']);
    this.sendNotification(NotificationType.SUCCESS, `You've been successfully logged out`);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  onUpdatePassage() {
    console.log("Hit")
    const formData = this.passageService.createPassageFormData(this.passage);
    this.subscriptions.push(
      this.passageService.updatePassage(this.user.username, this.passage.id, formData).subscribe(
        (event: HttpEvent<any>) => {
          this.clickButton('closeEditPassageModalButton');
          // clear
          this.sendNotification(NotificationType.SUCCESS, `updated successfully`);
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
          this.profileImage = null!;
        }
      )
    );
  }

  completePassage() {
    this.subscriptions.push(
      this.passageService.completePassage(this.user.username, this.passage.id).subscribe(
        (event: HttpEvent<any>) => {
          this.clickButton('closeCompletePassageModalButton');
          // clear
          this.sendNotification(NotificationType.SUCCESS, `Journey complete`);
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
          this.profileImage = null!;
        }
      )
    );
  }
}

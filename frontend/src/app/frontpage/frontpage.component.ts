import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {User} from "../model/user";
import {Router} from "@angular/router";
import {AuthenticationService} from "../service/authentication.service";
import {NotificationService} from "../service/notification.service";
import {BehaviorSubject, Subscription} from "rxjs";
import {MessageService} from "../service/message.service";
import {FormControl, FormGroup, Validator, Validators} from "@angular/forms";
import {Meta} from "@angular/platform-browser";
import {Yacht} from "../model/yacht";
import {NotificationType} from "../enum/notification-type.enum";
import {HttpErrorResponse} from "@angular/common/http";
import {YachtService} from "../service/yacht.service";
import {CaptainService} from "../service/captain.service";
import {Captain} from "../model/captain";
import {WebSocketService} from "../service/websocket.service";

export interface Message {
  type: string;
  message: string;
}

@Component({
  selector: 'app-frontpage',
  templateUrl: './frontpage.component.html',
  styleUrls: ['./frontpage.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})

export class FrontpageComponent implements OnInit, OnDestroy{

  //vars
  isOpen = false;
  loading = false;
  messages: Message[] = [];
  public yacht!: Yacht;
  public yachts!: Yacht[];
  public captain!: Captain;
  public captains!: Captain[];
  private subscriptions: Subscription[] = [];
  public selectedYacht: Yacht | undefined;
  public user!: User;
  public refreshing!: boolean;
  private titleSubject = new BehaviorSubject<string>('Beta testing is free to use!');
  public titleAction$ = this.titleSubject.asObservable();
  chatForm = new FormGroup({
    message: new FormControl('', [Validators.required])
  })
  postForm = new FormGroup({
    post: new FormControl('', [Validators.required])
  })
  posts: string[] = [];


  constructor(private meta: Meta, private router: Router, private authenticationService: AuthenticationService,
              private notificationService: NotificationService, private messageService: MessageService,
              public yachtService: YachtService, private captainService: CaptainService, public webService: WebSocketService) {
    this.messages.push({
      type: 'client',
      message: 'Hi, I am Calypso. How can I help you?',
    });

  }

  redirectToLogin(): void {
    if (!this.user) {
      this.router.navigate(['/login']);
    }
  }

  //open support pop up
  openSupportPopup() {
    this.isOpen = !this.isOpen;
  }

  // Send message
  sendMessage() {
    const sentMessage = this.chatForm.value.message!;
    this.loading = true;
    this.messages.push({
      type: 'user',
      message: sentMessage,
    });
    this.chatForm.reset();
    this.messageService.sendMessage(sentMessage).subscribe((response: any) => {
      this.loading = false;
      this.messages.push({
        type: 'client',
        message: response.message,
      });
    });
  }

  //Get yachts
  public getYachts(showNotification: boolean): void {
    this.refreshing = true;
    this.subscriptions.push(
      this.yachtService.getYachts().subscribe(
        (response: Yacht[]) => {
          this.yachtService.addYachtsToLocalCache(response);
          this.yachts = response.filter(yacht => parseInt(String(yacht.availableCabins)) >= 1); // Filter yachts with 1 or more available cabins
          this.refreshing = false;
          if (showNotification) {
            this.sendNotification(NotificationType.SUCCESS, `${this.yachts.length} yacht(s) loaded successfully.`);
          }
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, errorResponse.error.message);
          this.refreshing = false;
        }
      )
    );
  }


  // Search Yacht table
  public searchYachts(searchTerm: string): void {
    const results: Yacht[] = [];
    for (const yacht of this.yachtService.getYachtsFromLocalCache()) {
      // @ts-ignore
      if (yacht.name.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
        yacht.make.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
        yacht.model.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
        // @ts-ignore
        yacht.mmsi.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) {
        results.push(yacht);
      }
    }
    this.yachts = results;
    if (results.length === 0 || !searchTerm) {
      this.yachts = this.yachtService.getYachtsFromLocalCache();
    }
  }

  private clickButton(buttonId: string): void {
    // @ts-ignore
    document.getElementById(buttonId).click();
  }

  public onSelectYacht(selectedYacht: Yacht): void {
    this.selectedYacht = selectedYacht;
    this.clickButton('openYachtInfo');
  }


  private sendNotification(notificationType: NotificationType, message: string): void {
    if (message) {
      this.notificationService.notify(notificationType, message);
    } else {
      this.notificationService.notify(notificationType, 'An error occurred. Please try again.');
    }
  }

  connect(): void {
    this.webService.connect();
  }

  disconnect(): void {
    this.webService.disconnect();
  }

  sendYarn(): void {
    const post = (document.getElementById('post') as HTMLInputElement).value;
    this.webService.sendPost(post);
  }


  ngOnInit(): void {
    // call functions before the page loads
    this.user = this.authenticationService.getUserFromLocalCache();
    this.meta.addTag({ httpEquiv: 'Content-Security-Policy', content: 'upgrade-insecure-requests' });
  }

  ngOnDestroy(): void {
  }
}

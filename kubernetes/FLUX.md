## Flux GitLab bootstrap

```bash
flux check --pre
```
- Create access token to authenticate with the GitLab API. Tokens should be cycled
Installing and/or updating Flux System in cluster

```bash
export GITLAB_TOKEN=$Auth_Token_created`

flux bootstrap gitlab --personal --owner=pbates85 --repository=flux-devops --hostname=gitlab.com --branch=main --token-auth
```
Flux bootstrap populates the new repository with the flux2, kustomize, helm, and notification resources.
Additional manifests need to be created for flux to operate outside this repository. Staging-mymfg connects
flux to the repo containing resources as well as the kustomize controller how to use them.
Updating flux system or auth tokens can be done by rerunning the bootstrap.

Secret will need to be created for flux to interact with other repos besides the flux-fleet. The secret below will provide access to the staging kustomization repo and the secret ref is found in the staging-source yaml.
```bash
flux create secret git repo-auth --url=ssh://git@gitlab.com/pbates85/calypso.git
```
The ssh key output needs to be uploaded into GitLab
Once The kustomization repo can be read and written to the kustomization controller will read and create the resources and deployments. Once the resources have been deployed the Helm contoller will add the nginx ingress, falco security, and prometheus monitoring deployments.
### Flux repo source
```yaml
---
apiVersion: source.toolkit.fluxcd.io/v1beta2
kind: GitRepository
metadata:
  name: calypso-repo-source
  namespace: flux-system
spec:
  interval: 1m0s
  ref:
    branch: main
  secretRef:
    name:  repo-auth
  url: ssh://git@gitlab.com/pbates85/calypso.git
---
apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: calypso-repo-kustomizations
  namespace: flux-system
spec:
  # decryption:
  #   provider: sops
  #   secretRef:
  #     name: sops-gpg
  interval: 1m0s
  path: kubernetes/staging
  prune: true
  sourceRef:
    kind: GitRepository
    name: calypso-repo-source
  validation: client
```
In the event flux systems need to be uninstalled
```bash
flux uninstall --namespace=flux-system
```